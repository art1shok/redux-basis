export const incrementCounter = () => ({
  type: 'INCREMENT',
  // payload
});

export const decrementCounter = () => ({
  type: 'DECREMENT',
  //payload
});

const initialState = {
  count: 0,
  // user1: null,
  // user2: null,
};

// combineReducers()
export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'INCREMENT' :
      return { ...state, count: state.count + 1 };
    case 'DECREMENT' :
      return { ...state, count: state.count - 1 };
    default:
      return state;
  }
};
