import { useDispatch } from 'react-redux';
import { decrementCounter, incrementCounter } from './store/reducer';
import CountView from './components/countView/countView';

function App() {
  // const [count,setCount] = useState(0)
  const dispatch = useDispatch();
  const increment = () => {
    // setCount(count + 1);
    // dispatch(incrementCounter(1));
    dispatch(incrementCounter());
  }

  const decrement = () => {
    // setCount(count - 1);
    dispatch(decrementCounter());
    // dispatch(decrementCounter(-1));
  }

  return (
    <div className="App">
      <button type="button" onClick={increment}>
        Increment
      </button>
      <button type="button" onClick={decrement}>
        Decrement
      </button>
      <CountView />
      {/*<div>{count}</div>*/}
    </div>
  );
}

export default App;
