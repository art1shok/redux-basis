import { useSelector } from 'react-redux';

function CountView() {
  const count = useSelector((state) => state.count);
  // const count = useSelector((state) => state);

  console.log(count);

  return (
      <div>
        {count}
      </div>
  );
}

export default CountView;
